var ns = 'JAC-BRIDIE';
window[ns] = {};

// jQuery
// @codekit-prepend "../../bower_components/jquery/dist/jquery.min.js";

// Pikaday
// @codekit-prepend "../../bower_components/pikaday/pikaday.js";

// Used in swipe.srf.js
// @codekit-append "scripts/swipe.js"; 
// @codekit-append "scripts/swipe.srf.js"; 

// debounce - used by a bunch of things
// @codekit-append "scripts/debounce.js";
// @codekit-append "scripts/tim.microtemplate.js";

// @codekit-append "scripts/anchors.external.popup.js"; 
// @codekit-append "scripts/device.pixel.ratio.js";
// @codekit-append "scripts/standard.accordion.js";
// @codekit-append "scripts/responsive.video.js";

// @codekit-append "scripts/map/html.marker.js";
// @codekit-append "scripts/gmap.js";

// @codekit-append "scripts/aspect.ratio.js";
// @codekit-append "scripts/lazy.images.js";

// @codekit-append "scripts/search.js
// @codekit-append "scripts/nav.js
// @codekit-append "scripts/events.js
// @codekit-append "scripts/hero.js

(function($,context) {

			//event map
		$('button.view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});	

	$(document)
			.on('click','.inline-video',function(e) {
				var 
					el = $(this),
					src = el.data('video');
					
				if(src.length && !el.hasClass('inline-video-playing')) {
					el
						.addClass('inline-video-playing')
						.html('<iframe src="'+src+'"/>');
				}
				
			})			

}(jQuery,window[ns]));