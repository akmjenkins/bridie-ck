<?php

	/**
	
	
		NOTE: 
			You do not need to (and probably won't) be using this file, or a file like it in production.
			
			Your event source will probably be contained "within wordpress" somewhere, rather
			than within the template.

			It really doesn't make a difference, as long as the same
			json is returned when either an id parameter, or a month/year parameter is passed
			as arguments
	
	
	*/


//sample events
$events = array(
        array(
            'id' => '1',
            'start_time' => 1421424000,
            'end_time' => 1421434800,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-1',
            'title' => 'The Pints 1',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-2.jpg',
			'date_string' => 'Fri, Jan 16, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '2',
            'prev' => '',
        ),
        array(
            'id' => '2',
            'start_time' => 1421542400,
            'end_time' => 1421553200,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-2',
            'title' => 'The Pints 2',
			'date_string' => 'Sun, Jan 18, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '3',
            'prev' => '1',
        ),
        array(
            'id' => '3',
            'start_time' => 1421628800,
            'end_time' => 1421639600,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-3',
            'title' => 'The Pints 3',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-1.jpg',
			'date_string' => 'Mon, Jan 19, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '4',
            'prev' => '2',
        ),
        array(
            'id' => '4',
            'start_time' => 1421974400,
            'end_time' => 1421985200,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-4',
            'title' => 'The Pints 4',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-4.jpg',
			'date_string' => 'Fri, Jan 23, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '5',
            'prev' => '3',
        ),
        array(
            'id' => '5',
            'start_time' => 1422320000,
            'end_time' => 1422330800,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-5',
            'title' => 'The Pints 5',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-4.jpg',
			'date_string' => 'Tue, Jan 27, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '6',
            'prev' => '4',
        ),
        array(
            'id' => '6',
            'start_time' => 1422406400,
            'end_time' => 1422417200,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-6',
            'title' => 'The Pints 6',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-1.jpg',
			'date_string' => 'Wed, Jan 28, 2014',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 1,
			'year' => 2015,
            'next' => '7',
            'prev' => '5',
        ),
        array(
            'id' => '7',
            'start_time' => 1423011200,
            'end_time' => 1423022000,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-7',
            'title' => 'The Pints 7',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-3.jpg',
			'date_string' => 'Wed, Feb 4, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 2,
			'year' => 2015,
            'next' => '8',
            'prev' => '6',
        ),
        array(
            'id' => '8',
            'start_time' => 1423356800,
            'end_time' => 1423367600,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-8',
            'title' => 'The Pints 8',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-1.jpg',
			'date_string' => 'Sun, Feb 8, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 2,
			'year' => 2015,
            'next' => '9',
            'prev' => '7',
        ),
        array(
            'id' => '9',
            'start_time' => 1423875200,
            'end_time' => 1423886000,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-9',
            'title' => 'The Pints 9',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-1.jpg',
			'date_string' => 'Sat, Feb 14, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 2,
			'year' => 2015,
            'next' => '10',
            'prev' => '8',
        ),
        array(
            'id' => '10',
            'start_time' => 1424048000,
            'end_time' => 1424058800,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-10',
            'title' => 'The Pints 10',
			'date_string' => 'Mon, Feb 16, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 2,
			'year' => 2015,
            'next' => '11',
            'prev' => '9',
        ),
        array(
            'id' => '11',
            'start_time' => 1424566400,
            'end_time' => 1424577200,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-11',
            'title' => 'The Pints 11',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-1.jpg',
			'date_string' => 'Sun, Feb 22, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 2,
			'year' => 2015,
            'next' => '12',
            'prev' => '10',
        ),
        array(
            'id' => '12',
            'start_time' => 1425603200,
            'end_time' => 1425614000,
            'excerpt' => 'Vivamus posuere elementum leo ac feugiat. Morbi consectetur, quam sed tincidunt ultricies, libero felis semper nisi, quis facilisis erat ligula non enim. Pellentesque luctus mattis orci vitae tincidunt. Maecenas sollicitudin gravida nunc viverra feugiat.',
            'permalink' => 'http://bridiemolloys.com/events/the-pints-13',
            'title' => 'The Pints 13',
            'thumbnail' => 'http://dev.me/brid../assets/images/temp/event-image-4.jpg',
			'date_string' => 'Fri, Mar 6, 2015',
			'time_string' => '10:00PM - 1:00AM',
			'month' => 3,
			'year' => 2015,
            'next' => '',
            'prev' => '11'
        )
);
	
	class BridieEvents {
		
		public static function getEventById($id) {
			global $events;
			foreach($events as $event) {
				if($event['id'] === $id) {
					return $event;
				}
			}
			
			return array();
		}
		
		public static function getEventIdsForMonthYear($month,$year) {
			global $events;
			$eventData = array();
			
			
			
			foreach($events as $event) {
				$thisEventDate = new DateTime('@'.$event['start_time']);
				$thisEventMonth = $thisEventDate->format('n');
				$thisEventYear = $thisEventDate->format('Y');
				
				if($thisEventMonth === $month && $thisEventYear === $year) {
					$eventData[] = array(
						'id' => $event['id'],
						'day' => $thisEventDate->format('j')
					);
				}
				
			}
			
			return $eventData;
		}
		
	}
	
	header('Content-type: application/json');
	if(isset($_GET['id'])) {
		echo json_encode(BridieEvents::getEventById($_GET['id']));
	} else if(isset($_GET['month']) && isset($_GET['year'])) {
		echo json_encode(BridieEvents::getEventIdsForMonthYear($_GET['month'],$_GET['year']));
	} else {
		echo json_encode(array());
	}
	
	exit;