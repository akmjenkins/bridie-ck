			<footer>
				<div class="sw">
					<div class="footer-blocks d-bg">
					
						<div class="footer-block">
						
							<div class="footer-nav">
								<ul>
									<li><a href="#">Our Irish Pubs</a></li>
									<li><a href="#">Live Music</a></li>
									<li><a href="#">Menu</a></li>
									<li><a href="#">Promotions</a></li>
									<li><a href="#">Gallery</a></li>
									<li><a href="#">The Latest</a></li>
								</ul>
							</div><!-- .footer-nav -->
							
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="social-block">
								<?php include('i-social.php'); ?>
							</div><!-- .social-block -->
							
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="contact-block">
							
								<h6>Location</h6>
								<address>
									5 George Street <br />
									St. John's, NL A1B 2C3
								</address>
								
								<h6>Phone</h6>
								709 123-4567
								
							</div><!-- .contact-block -->
							
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Bridie Molloy's</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/bridie',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<!-- main.min.js includes jQuery/pikaday -->
		<script src="../assets/js/min/main-min.js"></script>
		
	</body>
</html>